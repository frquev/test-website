
const GROUND_HEIGHT = 600-100;
const NET_HEIGHT = 60;
const PLAYER_SIZE = 80;
const BALL_SIZE = 30;
const COLOR_P1 = "#F40100";
const COLOR_P2 = "#0006F2";

const MAX_HIT = 1;

//Colors
const COLOR_SKY = "#5C86FF";
const COLOR_BACK = "#3065FF";
const COLOR_GROUND = "#B58DA5";

//Keys
const key_a = 65;
const key_d = 68;
const key_w = 87;
const key_j = 74;
const key_l = 76;
const key_i = 73;

var p1_score = 0;
var p2_score = 0;
var p1hit = 0;
var p2hit = 0;

function setup() {
  createCanvas(800, 600);
  frameRate(144);

  p1_config = {
    x:100,
    color:COLOR_P1,
    left:key_a,
    right:key_d,
    up:key_w
  }
  
  p2_config = {
    x:width-100,
    color:COLOR_P2,
    left:key_j,
    right:key_l,
    up:key_i
  }

  player1 = new Player(p1_config);
  player2 = new Player(p2_config);
  ball = new Ball();
}

function draw() {
  drawBackground();

  player1.update();
  player2.update();
  ball.update(player1, player2);

  player1.show();
  player2.show();
  ball.show();

  if(ball.y >= GROUND_HEIGHT - BALL_SIZE/2){
    floorhit();
  }

  textSize(32);
  fill(player1.drawColor);
  text(p1_score, 100, 30);

  textSize(32);
  fill(player2.drawColor);
  text(p2_score, width-100, 30);
  //if (frameCount % 75 == 0) {
  //  pipes.push(new Pipe());
  //}
}

function keyPressed() {
  if (key == ' ') {
    player1.up();
    //console.log("SPACE");
  }
}

function drawBackground(){
    noStroke();
    fill(COLOR_SKY);
    rect(0, 0, width, height);
    fill(COLOR_BACK)
    rect(0, height - 300, width, height);
    fill(COLOR_GROUND)
    rect(0, GROUND_HEIGHT, width, height)
    fill(255)
    rect(width/2 - 2.5, GROUND_HEIGHT-NET_HEIGHT, 5, NET_HEIGHT);
}

function score(player){
  if(player == "p1"){
    p1_score += 1;
  }
  if(player == "p2"){
    p2_score += 1;
  }
  resetGame(player)
}

function resetGame(player){
  switch(player){
    case "p1":
      ball.x = p2_config.x;
    case "p2":
      ball.x = p1_config.x;
  }
  ball.speedY = 0;
  ball.speedX = 0;
  ball.y = GROUND_HEIGHT - 200;
  p1hit = 0;
  player1.x = p1_config.x;
  player2.x = p2_config.x;
}

function floorhit(){
  if(ball.x < width/2){
    p2hit = 0;
    p1hit += 1;
    if(p1hit > MAX_HIT){
      score("p2");
    }
  }
  if(ball.x > width/2){
    p2hit += 1;
    p1hit = 0;
    if(p2hit > MAX_HIT){
      score("p1");
    }
  }
}