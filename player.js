function Player(configs) {
    this.y = GROUND_HEIGHT;
    this.x = configs.x;
    this.drawColor = configs.color;
  
    this.gravity = 0.2;
    this.lift = -7;
    this.speedY = 0;
    this.speedX = 2;

    this.jumping = false;

    if (this.drawColor == COLOR_P1) {
      this.eye = 1;
    } else {
      this.eye = -1;
    }
    
  
    this.show = function() {
      fill(this.drawColor);
      arc(this.x, this.y, PLAYER_SIZE, PLAYER_SIZE, PI, 0);
      fill(255)
      ellipse(this.x + this.eye*PLAYER_SIZE/6, this.y - PLAYER_SIZE/3.5, PLAYER_SIZE/4);
      fill(0)
      ellipse(this.x + this.eye*PLAYER_SIZE/4.8, this.y - PLAYER_SIZE/3.5, PLAYER_SIZE/9);
    }
  
    this.up = function() {
      if (this.jumping == false) {
        this.speedY += this.lift;
        this.jumping = true;
      }
    };d

    this.moveleft = function() {
      if(((this.eye == 1) && (this.x - PLAYER_SIZE/2 > 0)) || ((this.eye == -1) && (this.x - PLAYER_SIZE/2 > width/2 + 2.5))) {
        this.x -= this.speedX;
      }
    };

    this.moveright = function() {
      if(((this.eye == 1) && (this.x + PLAYER_SIZE/2 < width/2-2.5)) || ((this.eye == -1) && (this.x + PLAYER_SIZE/2 < width))) {
        this.x += this.speedX;
      }
    };
  
    this.update = function() {
      if (keyIsDown(configs.left)) {this.moveleft()}
      if (keyIsDown(configs.right)) {this.moveright()};
      if (keyIsDown(configs.up)) {this.up()};

      this.speedY += this.gravity;
      this.y += this.speedY;
  
      if (this.y >= GROUND_HEIGHT) {
        this.y = GROUND_HEIGHT;
        this.speedY = 0;
        this.jumping = false;
      }
  
      if (this.y < 0) {
        this.y = 0;
        this.speedY = 0;
        this.jumping = false;
      }
  
    }
  
  }