function Ball() {
    this.y = GROUND_HEIGHT-200;
    this.x = 100;
  
    this.gravity = 0.05;
    this.lift = -7;
    this.speedY = 0;
    this.speedX = 0;

    this.d1 = 0;
    this.d2 = 0;
    this.direction = 0;
    this.angle = 0;
    this.speed = 1.5;
  
    this.show = function() {
      fill(255);
      ellipse(this.x, this.y, BALL_SIZE);
    }
  
    this.up = function() {
    };

    this.moveleft = function() {
    };

    this.moveright = function() {
    };
  
    this.update = function(p1, p2) {
        this.d1 = Math.sqrt((this.x - p1.x)**2 + (this.y - p1.y)**2);
        this.d2 = Math.sqrt((this.x - p2.x)**2 + (this.y - p2.y)**2);

        this.speedY += this.gravity;
        this.y += this.speedY;
        this.x += this.speedX

        //this.speed = Math.sqrt(this.speedX*this.speedX + this.speedY*this.speedY)/3;

        if(this.d1 <= PLAYER_SIZE/2 + BALL_SIZE/2) {
            this.angle = Math.atan2(this.y - p1.y, this.x - p1.x);

            this.speedY = 0.8*(this.speed*(Math.sqrt(p1.speedX*p1.speedX + p1.speedY*p1.speedY)))*Math.sin(this.angle);
            this.speedX = 0.8*(this.speed*(Math.sqrt(p1.speedX*p1.speedX + p1.speedY*p1.speedY)))*Math.cos(this.angle);
        }

        if(this.d2 <= PLAYER_SIZE/2 + BALL_SIZE/2) {
            this.angle = Math.atan2(this.y - p2.y, this.x - p2.x);

            this.speedY = 1.5*(Math.sqrt(p2.speedX*p1.speedX + p2.speedY*p2.speedY))*Math.sin(this.angle);
            this.speedX = 1.5*(Math.sqrt(p2.speedX*p1.speedX + p2.speedY*p2.speedY))*Math.cos(this.angle);
        }

        if ((this.x >= width - BALL_SIZE/2) || (this.x < 0 + BALL_SIZE/2)) {
            this.speedX *= -1;
        }

        if (this.y >= GROUND_HEIGHT - BALL_SIZE/2) {
        this.y = GROUND_HEIGHT - BALL_SIZE/2;
        this.speedY *= -1;
        this.jumping = false;
        }

        if (this.y + BALL_SIZE/2 >= GROUND_HEIGHT - NET_HEIGHT) {
            var Lpos = this.x + BALL_SIZE/2;
            var Rpos = this.x - BALL_SIZE/2;
            if(((Lpos < width/2) && (Lpos >= width/2 - 2.5)) || ((Rpos > width/2) && (Rpos <= width/2 + 2.5))){
                this.speedX *= -1;
            }
            if((this.x > width/2 - BALL_SIZE/2) && (this.x < width/2 + BALL_SIZE/2)){
                this.speedY *= -1;
            }
        }

        if (this.y < 0) {
            this.y = 0;
            this.speedY = 0;
            this.jumping = false;
        }
        
        this.speedY *= 0.994;
        this.speedX *= 0.998;
        console.log(this.speedY);
    }
  
  }